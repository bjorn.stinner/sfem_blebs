########################################################
# required packages 
########################################################

import os
from dune.alugrid import aluSimplexGrid 
from dune.fem.view import geometryGridView
from dune.fem.space import lagrange
from blebbing_tools import surfaceOptions
from blebbing_compute import compute

########################################################
# data and parameters for a specific simulation 
########################################################

# initial surface,
# reading a mesh file in the dune grid format (dgf, see DUNE documentation),
# here a mesh file for a sphere that then is deformed
gridSelect = "sphere.dgf"
global_refinements = 3  # number of global mesh refinements performed at the beginning
case = 4 # this number selects the above presented surfaceOption 

# base set of non dimensional parameters, see documentation for the model
class Param:
    x_0 = 0.8
    l_b = 0.005
    l_l = 18.0
    l_0 = 0.04
    u_B = 0.056
    k_L = 500.0
    u_R = 0.0075
    l_p = 60.0
    # time discretisation parameters
    deltaT = 0.001
    finalT = 10.0
# addition for singal model (zero diffusion and production)
class ParamSignal(Param):
    l_L = 450.0
    c_b = 0.2
    c_B = 1
    l_c = 1.2*c_B
    d_f = 1
    D_c = 0
    r_c = 0

# parameters for 5.5 with uniform diffusion
class Param1(ParamSignal):
    d_f = 1
    D_c = 10
    r_c = 12.0
# parameters for 5.5 with reduced diffusion
class Param2(ParamSignal):
    d_f = 0.01
    D_c = 10
    r_c = 12.0

Models = [Param1,Param2]

# specifying the output 
# 2: outputs both the deformation u and the curvature w over the grid
outputFlag = 2
deltaVis = 0.25 # visualisation time step
pol_order = 1   # polynomial degree of the spatial approximation 

########################################################
# generate the grid 
########################################################

# here, the alugrid grid manager is used
# the vertices then are moved as specified above
# for that purpose, a finite element space on the undeformed grid is required
# and then a geometryGridView generated that accounts for the initial deformation
rawGrid = aluSimplexGrid(gridSelect, dimgrid=2, dimworld=3)
global_refinements = global_refinements * rawGrid.hierarchicalGrid.refineStepsForHalf
rawGrid.hierarchicalGrid.globalRefine(global_refinements)
rawSpace = lagrange(rawGrid, dimRange=3, order=pol_order)
vertices = rawSpace.interpolate(surfaceOptions[case], name="vertices")
surfaceGrid = geometryGridView(vertices)

########################################################
# call the routine with the model and the solvers
########################################################
for Model in Models:
    print("computing with parameters from",Model.__name__)
    os.makedirs(Model.__name__, exist_ok=True)
    oFileName  = Model.__name__+"/bleb" 
    compute(surfaceGrid,Model,pol_order,outputFlag,oFileName,deltaVis)
