########################################################
# required packages 
########################################################

import os
# this grid was used for the simulations in Section 5.3 of the paper
#   from dune.alugrid import aluConformGrid as Grid 
# to unify with the other simulations we switch here to a grid manager
# that uses quartering instead of bisection
from dune.alugrid import aluSimplexGrid as Grid
from dune.fem.view import geometryGridView
from dune.fem.space import lagrange
from blebbing_tools import surfaceOptions
from blebbing_compute import compute

########################################################
# data and parameters for a specific simulation 
########################################################

# initial surface,
# reading a mesh file in the dune grid format (dgf, see DUNE documentation),
# here a mesh file for a sphere that then is deformed
gridSelect = "sphere2b.dgf"

# number of global mesh refinements performed at the beginning each halfing h
global_refinements = 7 # with aluGridConform this leads to 14 bisection refinenment steps
# this number selects the above presented surfaceOption 
case = 2

# physical parameters, see documentation for the model
class Param:
    x_0 = 0.95
    l_b = 0.005
    l_l = 18.0
    l_0 = 0.04
    u_B = 0.056
    k_L = 500.0
    u_R = 0.0075
    l_p = 22.5
    # time discretisation parameters
    deltaT = 0.001
    finalT = 2.0

Models = [Param]

# specifying the output 
# 2: outputs both the deformation u and the curvature w over the grid
outputFlag = 2
oFileName  = "bleb_artgeom" 
deltaVis = 0.25 # visualisation time step
pol_order = 1   # polynomial degree of the spatial approximation 

########################################################
# generate the grid 
########################################################

# here, the alugrid grid manager is used
# the vertices are moved as specified above
# for that purpose, a finite element space on the undeformed grid is required
# and then a geometryGridView generated that accounts for the initial deformation
rawGrid = Grid(gridSelect, dimgrid=2, dimworld=3)
global_refinements = global_refinements * rawGrid.hierarchicalGrid.refineStepsForHalf
rawGrid.hierarchicalGrid.globalRefine(global_refinements)
rawSpace = lagrange(rawGrid, dimRange=3, order=pol_order)
vertices = rawSpace.interpolate(surfaceOptions[case], name="vertices")
surfaceGrid = geometryGridView(vertices)

########################################################
# call the routine with the model and the solvers
########################################################

for Model in Models:
    print("computing with parameters from",Model.__name__)
    os.makedirs(Model.__name__, exist_ok=True)
    oFileName  = Model.__name__+"/bleb" 
    compute(surfaceGrid,Model,pol_order,outputFlag,oFileName,deltaVis)
