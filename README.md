# Cell blebbing with surface finite elements

Software accompanying the paper 'A surface finite element method for computational modelling of cell blebbing' by B Stinner, A Dedner, and A Nixon. 

The project uses the Python bindings available for DUNE-FEM, for further details including installation instructions see here:
https://dune-project.org/sphinx/content/sphinx/dune-fem/

The easiest way to get started is to run the `startdune.sh` script located
in this repository. This will download the `dune-fem` docker container (can
take some time). Once inside the container the Python scripts included in
this repository can be executed. The first time the examples are run
requires additional compilation so will also take some time - subsequent
runs are then much faster.

*Two computational examples* are provided to reproduce the simulations in the paper (up to some parameter variants). They can be run with 
`python blebbing_artgeom.py` (artificial, given initial geometry) and `python blebbing_imgdata.py` (initial mesh obtained from image data), respectively. 
In these two python files the parameters a fixed and the initial surface is generated from some grid files, which are `sphere2b.dgf` for `blebbing_artgeom.py` and `cell.dgf` for `blebbing_imgdata.py`. 
After, a compute routine is called. 
The file `blebbing_compute.py` contains this compute routine. It comprises the definition of the model, the finite element setup, numerical schemes, a time stepping routine, and the solver for each time step. 
The file `blebbing_tools.py` contains some additional functions and tools such as deformation functions to generate initial shapes or nonlinear terms for the variational model. 
In both examples, the output consists of some .vtu files that can be visualised with paraview. 

