rundune="https://gitlab.dune-project.org/dune-fem/dune-fem-dev/-/raw/master/rundune.sh"
wget -O .rundune.sh -q $rundune > /dev/null
if [ ! -f .rundune.sh ]; then
  echo "Could not download the 'rundune.sh' script - do you have internet access?"
else
  source ./.rundune.sh
fi
