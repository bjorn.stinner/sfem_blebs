########################################################
# required packages 
########################################################

import os
from dune.alugrid import aluSimplexGrid 
from blebbing_compute import compute

########################################################
# data and parameters for a specific simulation 
########################################################

# initial surface,
# reading a mesh file in the dune grid format (dgf, see DUNE documentation),
# here a file obtained from image data
gridSelect = "cell.dgf"
global_refinements = 0  # number of global mesh refinements performed at the beginning

# physical parameters, see documentation for the model
class ImgData:
    l_b = 0.125
    x_0 = 0.95
    l_l = 0.72
    u_B = 0.28
    k_L = 500.0
    u_R = 0.15
    l_0 = 0.2
    l_p = 150.0
    # time discretisation parameters
    deltaT = 0.02 
    finalT = 20.02

Model = ImgData

# specifying the output 
# 2: outputs both the deformation u and the curvature w over the grid 
outputFlag = 2
oFileName  = "bleb_imgdata" 
deltaVis = 2.0 # visualisation time step 
pol_order = 1  # polynomial degree of the spatial approximation 

########################################################
# generate the grid 
########################################################

# generate a grid from the dgf file and perform some global refinements
# here, the alugrid grid manager is used
surfaceGrid = aluSimplexGrid(gridSelect, dimgrid=2, dimworld=3)
global_refinements = global_refinements * surfaceGrid.hierarchicalGrid.refineStepsForHalf
surfaceGrid.hierarchicalGrid.globalRefine(global_refinements)

########################################################
# call the routine with the model and the solvers
########################################################

print("computing with parameters from",Model.__name__)
os.makedirs(Model.__name__, exist_ok=True)
oFileName  = Model.__name__+"/bleb" 
compute(surfaceGrid,Model,pol_order,outputFlag,oFileName,deltaVis)
