import math
import numpy
from dune.common import FieldVector
from ufl import *

# useful functions and tools for the nonlinear terms

def computeArea(surface):
    totalArea = 0
    for element in surface.elements:
        totalArea += element.geometry.volume
    return totalArea

def norm(u):
    return sqrt(sum([x*x for x in u]))

def invNormNxN(eta):
    S = 1/sqrt(inner(grad(eta),grad(eta)))
    return S
# regularisation would read
#    eps = 1.0e-5
#    return 1/sqrt(S + \eps)

def invVol(vol):
    return 1.0/vol
# regularisation would read
#    eps = 1.0e-5
#    return 1/(vol + eps)

def cortexCalc(u_0,L_0,n_0):
    # computes the cortex position u_c = u_0 - L_0 n_0
    return as_vector( [ u_0[i] - L_0*n_0[i] for i in range(3) ] )

def cortexDistVec(u,u_0,L_0,n_0):
    # computes u - u_c
    y = cortexCalc(u_0,L_0,n_0)
    return as_vector([ u[i] - y[i] for i in range(3) ])

def elNormal(e,hatx):
    w1 = e.geometry.corners[1] -  e.geometry.corners[0]
    w2 = e.geometry.corners[2] -  e.geometry.corners[0]
    Normal_vec  = FieldVector([w1[1]*w2[2] - w1[2]*w2[1],
                               w1[2]*w2[0] - w1[0]*w2[2],
                               w1[0]*w2[1] - w1[1]*w2[0]])
    return Normal_vec / Normal_vec.two_norm

def reaction_signal(s,l_c):
    return as_vector([ l_c - s[0] ])

# functions for deforming the initial shape,
# used for computations with artificial geometry

def identity(x):
    return x

def unit_sphere(x):
    y=[0,0,0]
    r1 = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])
    r0 = 1.
    y[0] = r0*x[0]/r1
    y[1] = r0*x[1]/r1
    y[2] = r0*x[2]/r1
    return y

def discocyte(x):
    y=[0,0,0]
    r1 = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])
    r0 = 4.
    y[0] = r0*x[0]/r1
    y[1] = r0*x[1]/r1
    r2 = sqrt(x[0]*x[0]+x[1]*x[1])
    r3 = sqrt(y[0]*y[0]+y[1]*y[1])
    y[2] = 1.5-0.5*cos(pi*r3/2.)
    if r3>2.:
        y[2]=sqrt(4.-(r3-2.)*(r3-2.))
    if x[2]<0.:
        y[2]=-y[2]
    return y

def pear_shape(x):
    y=[0,0,0]
    r1 = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])
    r0 = 4.
    y[0] = r0*x[0]/r1
    y[1] = r0*x[1]/r1
    r3 = sqrt(y[0]*y[0]+y[1]*y[1])
    y[2]= r0*x[2]/r1
    if(y[2]>0):    
        if r3<2.5:
            y[2]= sqrt(r0*r0-2.5*2.5)+1.5+1.5*cos(pi*pow(r3*r3/(2.5*2.5),0.75))    
    return y

def non_axisym(x):
    r1 = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])
    r0 = 4.
    xn = [0,0,0]
    y = [0,0,0]
    xn[0] = x[0]/r1
    y[0] = r0*x[0]/r1
    xn[1] = x[1]/r1
    y[1] = r0*x[1]/r1
    xn[2] = x[2]/r1
    if x[2]<0.:
        y[2] = r0*x[2]/r1
    else:
        r3 = sqrt(y[0]*y[0]+y[1]*y[1])
        y[2] = 1.7-0.3*cos(pi*r3/2.)
        if r3>2.:
            y[2]=sqrt(4.-(r3-2.)*(r3-2.))
    z = complex(xn[0],xn[1])
    theta = numpy.angle(z)
    fac = 1. + 0.3*(1-xn[2]*xn[2])*cos(3*theta)
    res = [0,0,0]
    res[0] = fac*y[0]
    res[1] = fac*y[1]
    res[2] = fac*y[2]
    return res

surfaceOptions = { 0 : identity ,
                   1 : unit_sphere,
                   2 : discocyte,
                   3 : pear_shape,
                   4 : non_axisym
                 }
