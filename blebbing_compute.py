from dune.fem.space import lagrange, finiteVolume # dglagrange
from dune.fem.function import integrate
from dune.fem.operator import galerkin as galerkinOperator
from dune.fem.operator import linear as linearOperator
from dune.fem.scheme import galerkin as galerkinScheme
from ufl import *
from dune.ufl import Constant
from blebbing_tools import computeArea, norm, invNormNxN, invVol, cortexCalc, cortexDistVec, elNormal, reaction_signal 

def compute(surfaceGrid,Param,polOrder,outputFlag,oFileName,deltaVis):
    # extract static parameters from parameters class
    # time discretisation parameters
    deltaT  = Param.deltaT
    finalT  = Param.finalT

    # set up finite elements
    solutionSpace = lagrange(surfaceGrid, dimRange=3, order=polOrder, storage="istl")
    solutionSpace_c = lagrange(surfaceGrid, dimRange=1, order=polOrder, storage="istl")

    # the position is initialised with the vertex positions, the curvature and c with zero
    position  = solutionSpace.interpolate(lambda x: x, name="position")
    curvature = solutionSpace.interpolate([0,]*solutionSpace.dimRange, name="curvature")
    signal = solutionSpace_c.interpolate([0,]*solutionSpace_c.dimRange, name="c")

    # unit normal field
    normalSpace = finiteVolume(surfaceGrid, dimRange=3, storage="istl")
    normal_0 = normalSpace.interpolate(elNormal, name="normal")
    surfaceGrid.writeVTK(Param.__name__+"/normal", celldata=[normal_0], number=0)

    # initial graphical output, deformation can be used to warp the mesh
    visCounter = 0
    if outputFlag==2:
        deformation = solutionSpace.interpolate([0,]*solutionSpace.dimRange, name="deformation")
        surfaceGrid.writeVTK(oFileName, pointdata=[curvature,deformation,signal], number=visCounter)
        visCounter = visCounter + 1

    # discrete function for the actual position and c (needed for the time stepping scheme)
    # and the initial position (needed for the cortex position, the unit normal etc)
    position_n = position.copy()
    position_0 = position.copy()
    signal_n = signal.copy()

    ########################################################
    # model, contributions to the variational problem
    ########################################################

    # compute and output the cell's surface area and volume
    # the latter is used in the model
    totalArea = computeArea(surfaceGrid)
    print("Area: ", totalArea)
    vol_fct = (1.0/3) * inner(normal_0, position) 
    Val_iVl = integrate(surfaceGrid, vol_fct, order=1)
    print("Volume: ", Val_iVl)

    # fields
    u   = TrialFunction(solutionSpace)
    phi = TestFunction(solutionSpace)
    w   = TrialFunction(solutionSpace)
    eta = TestFunction(solutionSpace)
    c   = TrialFunction(solutionSpace_c)
    z   = TestFunction(solutionSpace_c)
    
    def setConstant(attribute, default=None):
        return Constant(getattr(Param,attribute),attribute) if default is None else \
               Constant(getattr(Param,attribute,default),attribute)
    # parameter, discretisation
    tau = Constant(deltaT, "tau")
    iVl = Constant(Val_iVl, "iVl")
    # parameter, physical
    l_b = setConstant("l_b")
    x_0 = setConstant("x_0")
    l_l = setConstant("l_l")
    u_B = setConstant("u_B")
    k_L = setConstant("k_L")
    u_R = setConstant("u_R")
    l_0 = setConstant("l_0")
    l_p = setConstant("l_p")
    # parameter, signal
    l_L = setConstant("l_L", Param.l_l)
    c_b = setConstant("c_b", 0)
    c_B = setConstant("c_B", 1)
    l_c = setConstant("l_c", 0)
    D_c = setConstant("D_c", 0)
    r_c = setConstant("r_c", 0)
    d_f = setConstant("d_f", 1)

    # terms that feature in the model, 
    # _im stands for implicit (in time) terms,
    # _ex stands for explicit (in time) terms;
    # see blebbing_tools.py for nonlinear terms and usefuls tools

    cubic = lambda x: (x-c_b)**2 * (3*c_B-c_b - 2*x) / (c_B-c_b)**3
    weight = lambda x: conditional(x<c_b,0, conditional(x>c_B,1,cubic(x)) )

    # bending terms with operator splitting
    bending_im = l_b * inner(grad(w), grad(phi))
    op_split_pos_im = inner(grad(u), grad(eta))
    op_split_curv_im = -inner(w, eta)
    # tension terms
    lfac = x_0 # + (0.95-x_0)*weight(signal_n[0])
    tension_im = inner(grad(u), grad(phi)) 
    tension_ex = sqrt(2.0) * lfac * invNormNxN(position_n) * inner(grad(position_n), grad(phi)) 
    # pressure term
    lfac = l_p # * (1 - 0.6*weight(signal_n[0]))
    pressure = lfac * invVol(iVl) * inner(normal_0, phi)
    # linker terms
    connection  = conditional(norm(cortexDistVec(position_n, position_0, l_0, normal_0)) < u_B, 1,     0)
    compression = conditional(norm(cortexDistVec(position_n, position_0, l_0, normal_0)) < u_R, k_L+1, 1)
    # could be smoothed, too
    corDistVec = cortexDistVec(position_n, position_0, l_0, normal_0)
    NuC = (1.0/norm(corDistVec)) * corDistVec
    lfac = l_l + (l_L-l_l)*weight(signal_n[0])
    linkers_im = lfac * connection * compression * inner(u, phi)
    linkers_ex = lfac * connection * compression * inner(cortexCalc(position_0, l_0, normal_0) + l_0 * NuC, phi)
    # signal terms
    detached = conditional(norm(corDistVec) < u_B, 0, 1)
    signalTerms_im = D_c * (detached+(1-detached)*d_f) * inner(grad(c), grad(z)) 
    signalTerms_ex = r_c * detached * inner(reaction_signal(signal_n,l_c),z)

    # models
    mainPosModel = ((inner(u,phi) + tau * (tension_im + linkers_im)) - (inner(position_n, phi) + tau * (tension_ex + pressure + linkers_ex))) * dx
    curvModel = tau * bending_im * dx
    opSplitPosModel = op_split_pos_im * dx
    opSplitCurvModel = op_split_curv_im * dx
    mainSigModel = ((inner(c,z) + tau * signalTerms_im) - (inner(signal_n,z) + tau * signalTerms_ex)) * dx
    
    ########################################################
    # schemes and operators 
    ########################################################

    # operators and corresponding matrices
    curvOp = galerkinOperator(curvModel)
    # curvOp = galerkinOperator(curvModel, coefficients={u_n:position_n, u_0:position_0})
    matCurvOp = linearOperator(curvOp)

    stiffOp = galerkinOperator(opSplitPosModel)
    matStiff = linearOperator(stiffOp)

    massOp = galerkinScheme(-opSplitCurvModel==0)
    matMass = linearOperator(massOp)
    innersolver = {"method":"cg", "tolerance":1e-8, "verbose":False,
                   "preconditioning.method":"ilu"}
    matMassInv = massOp.inverseLinearOperator(matMass, parameters=innersolver)

    posOp = galerkinScheme(mainPosModel==0)
    matPosOp = linearOperator(posOp)

    # using the Schur complement, 
    # the problem for the position reads 
    # (posOp + curvOP * massInvOp * stiffOP) position = 0

    # finally the scheme for the signal diffusion-reaction equation
    sigOp = galerkinScheme(mainSigModel==0, solver="cg",
                 parameters={"newton.tolerance":1e-6,
                             "newton.linear.tolerance":1e-8,
                             "newton.linear.preconditioning.method":"amg-ilu"})

    # the problem for the signal field reads 
    # sigOp signal = 0

    ########################################################
    # time loop, cg iteration within each time step 
    ########################################################

    # for the cg iteration some parameters storage vectors are required
    tolCG = 1.0e-6 # CG terminates of the residual is small enough
    max_iter = 500 # at most max_iter CG steps
    resid = curvature.copy() # residual
    direc = curvature.copy() # search direction

    # also for the matrix-vector multiplication 
    # some storage vectors are required
    hilf_opsplit1 = curvature.copy()
    hilf_opsplit2 = curvature.copy()
    hilf = curvature.copy()

    steps = int(finalT/deltaT)
    visNext = deltaVis
    actTime = 0.0
    print("begin time stepping", flush=True )
    for n in range(1, steps+1):
        # copy actual position and signal into old position and signal
        position_n.assign(position)
        signal_n.assign(signal)

        # computing first residual
        matStiff(position, hilf_opsplit1)
        hilf.clear()
        matMassInv(hilf_opsplit1, hilf)
        matCurvOp(hilf, hilf_opsplit2)
        posOp(position, resid)
        resid *= -1
        resid -= hilf_opsplit2

        # terminate if the norm of the residual is below the tolerance
        delta = resid.scalarProductDofs(resid)
        norm_resid = sqrt(delta)
        # print("norm_resid: ", norm_resid)
        if (norm_resid>tolCG):
            # first search direction is the residual
            direc.assign(resid)
            # print("begin cg iteration" )

            # CG iteration
            for m in range(max_iter):
                # apply the system matrix to the search direction
                matStiff(direc, hilf_opsplit1)
                hilf.clear()
                matMassInv(hilf_opsplit1, hilf)
                matCurvOp(hilf, hilf_opsplit2)
                matPosOp(direc, hilf)
                hilf += hilf_opsplit2

                # update the position and the residual
                alpha = delta / direc.scalarProductDofs(hilf)
                position.axpy(alpha, direc)
                resid.axpy(-alpha, hilf)

                # terminate if the norm of the residual is below the tolerance
                delta_old = delta
                delta = resid.scalarProductDofs(resid)
                norm_resid = sqrt(delta)
                # print("norm_resid: ", norm_resid)
                if (norm_resid<=tolCG): break

                if (m<max_iter-1):
                    # otherwise, compute next search direction
                    beta = delta / delta_old
                    hilf.assign(direc)
                    direc.assign(resid)
                    direc.axpy(beta, hilf)
            if m == max_iter:
              print("Warning: cg terminated by reaching the maximal number of iterations. The norm of the residual is ", norm_resid, flush=True)
        else:
            print("Warning: The norm of the residual ", norm_resid, " was below the tolerance - no cg iteration done", flush=True)

        # compute curvature
        matStiff(position, hilf_opsplit1)
        matMassInv(hilf_opsplit1, curvature)

        # update signal field
        sigOp.solve(target=signal)

        # update time and output if desired
        actTime = n * deltaT
        if visNext <= actTime:
            if outputFlag==2:
                deformation.assign(position)
                deformation.axpy(-1.0, position_0)
                surfaceGrid.writeVTK(oFileName, pointdata=[curvature,deformation,signal], number=visCounter)
            visNext = visNext + deltaVis
            visCounter = visCounter + 1
        print(actTime,flush=True)
